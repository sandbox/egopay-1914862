Description
EgoPay module is a reliable shopping software for online stores, intended to
 attract more shoppers and to increase online sales. EgoPay module guarantees 
safe, quick and easy integration process and ensures quality shopping experience
 for your customers. Many different e-commerce and open-source shopping carts 
all over the world use EgoPay module every day due to it‘s stability. And it is
 free! Merchants can sell goods/services online and accept e-currency payments
 via EgoPay - www.egopay.com . They are able to have multiple wallets in EgoPay 
and can accept payments to the chosen wallet. In order to do so, they have to
 use their store ID and password. All customers need to do is create an account
 in EgoPay to make payments. EgoPay provides a secure environment to protect all
 sensitive information using advanced 256 bit SSL encryption provided by 
GeoTrust Inc. and secure IPN handling environment, so merchants can feel 
absolutely safe using EgoPay payments module. Also, hard working EgoPay team
 monitor payments 24 hours 7 days a week. 
Features:
1. Quickly and easily configurable.
2. Unique secure ipn handling.
3. Installation procedures follow standard extension's setup guidelines.
4. Full operations can be done only by using EgoPay store ID and store password.
5. Only EgoPay login details are required to make a payment.
6. Customer is automatically redirected to egopay.com website to submit a 
payment.
7. Instant notifications when payment is recieved, holded or canceled.
8. Automatic order status update.
9. Order history updates (optional).
10. Payment history updates (optional).
11. Payments between EgoPay accounts are instant.
12. All transactions are backed by USD and EUR.

Installation and Configuration:
Install the Egopay module.
Enable the Egopay payment method rule via Store Settings -> Payment methods and 
edit its action. Enter your unique store id and store password and provide the
 default currency that your store will be using. Currently transactions are 
available in only two currencies (USD and EUR). Save your configuration.
