<?php

/**
 * @file
 * Implements Egopay payment services for use with Drupal Commerce.
 */

/**
 * Loads egopaySci library
 */
function commerce_egopay_load_egopay_sci_library()
{
    if (file_exists(dirname(__FILE__) . '/egopaysci.php')) {
        require(dirname(__FILE__) . '/egopaysci.php');
    }
}

/**
 * Implements hook_menu().
 */
function commerce_egopay_menu()
{
    $items = array();

    // Define an always accessible path to receive IPNs.
    $items['commerce_egopay/ipn'] = array(
        'page callback' => 'commerce_egopay_process_ipn',
        'page arguments' => array(),
        'access callback' => true,
        'type' => MENU_CALLBACK,
    );

    // Define an additional IPN path that is payment method / instance specific.
    $items['commerce_egopay/ipn/%commerce_payment_method_instance'] = array(
        'page callback' => 'commerce_egopay_process_ipn',
        'page arguments' => array(2),
        'access callback' => true,
        'type' => MENU_CALLBACK,
    );

    return $items;
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_egopay_commerce_payment_method_info()
{
    $payment_methods = array();

    $payment_methods['egopay'] = array(
        'base' => 'commerce_egopay',
        'title' => t('Egopay'),
        'short_title' => t('Egopay'),
        'display_title' => t('Egopay'),
        'description' => t('Egopay e-payments'),
        'terminal' => false,
        'offsite' => true,
        'offsite_autoredirect' => true,
    );

    return $payment_methods;
}

/**
 * Returns the default settings for Egopay payment method.
 */
function commerce_egopay_default_settings()
{
    $default_currency = variable_get('commerce_default_currency', 'USD');

    return array(
        'egopay_store_id' => '123456789',
        'egopay_store_password' => '123456789',
        'currency_code' => 'USD',
    );
}

/**
 * Payment method callback: settings form.
 */
function commerce_egopay_settings_form($settings = array())
{
    $form = array();

    $settings = (array)$settings + commerce_egopay_default_settings();

    $form['egopay_store_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Egopay store id'),
        '#description' => t('Egopay unique store id which can be found at Egopay.com'),
        '#default_value' => $settings['egopay_store_id'],
        '#required' => true,
    );

    $form['egopay_store_password'] = array(
        '#type' => 'textfield',
        '#title' => t('Egopay store password'),
        '#description' => t('Egopay store password which can be found at Egopay.com'),
        '#default_value' => $settings['egopay_store_password'],
        '#required' => true,
    );

    $form['currency_code'] = array(
        '#type' => 'select',
        '#title' => t('Default currency'),
        '#description' => t('Transactions in other currencies will be converted to this currency, so multi-currency sites must be configured to use appropriate conversion rates.'),
        '#options' => commerce_egopay_currencies(),
        '#default_value' => $settings['currency_code'],
    );

    return $form;
}

/**
 * Payment method callback: redirect form, a wrapper around the module's general
 *   use function for building a WPS form.
 */
function commerce_egopay_redirect_form($form, &$form_state, $order, $payment_method)
{
    commerce_egopay_load_egopay_sci_library();
    // Return an error if the enabling action's settings haven't been configured.
    if (empty($payment_method['settings']['egopay_store_id'])) {
        drupal_set_message(t('Egopay store id is not specified'), 'error');
        return array();
    }

    if (empty($payment_method['settings']['egopay_store_password'])) {
        drupal_set_message(t('Egopay store password is not specified'), 'error');
        return array();
    }

    $settings = array(
        // Return to the previous page when payment is canceled
        'fail_url' => url('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key'], array('absolute' => true)),
        // Return to the payment redirect page for processing successful payments
        'success_url' => url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => true)),
        // Specify the current payment method instance ID in the notify_url
        'payment_method' => $payment_method['instance_id'],
    );

    return commerce_egopay_order_form($form, $form_state, $order, $payment_method['settings'] + $settings);
}

/**
 * Payment method callback: process an IPN once it's been validated.
 */
function commerce_egopay_process_ipn($payment_method = null, $debug_ipn = array())
{
    commerce_egopay_load_egopay_sci_library();

    if (empty($_POST['product_id'])) {
        watchdog('commerce_egopay', 'IPN URL accessed with no POST data submitted.', array(), WATCHDOG_WARNING);
        return false;
    }

    $product_id = trim($_POST['product_id']);
    $product['product_id'] = htmlspecialchars($product_id);

    $EgoPay = new EgoPaySciCallback(array(
        'store_id' => $payment_method['settings']['egopay_store_id'],
        'store_password' => $payment_method['settings']['egopay_store_password']
    ));

    if (function_exists('curl_version')) {
        $data = $EgoPay->getResponse($product);

        if (!empty($data['cf_1'])) {
            $order = commerce_order_load($data['cf_1']);
        } else {
            $order = false;
        }

        if (empty($data['sId'])) {
            return false;
        }

        if (!in_array($data['sStatus'], array('Completed', 'On Hold', 'Pending', 'Canceled'))) {
            commerce_payment_redirect_pane_previous_page($order);
            return false;
        }

        $transactions = commerce_payment_transaction_load_multiple(array(), array('remote_id' => $data['sId']));
        $transaction = array_shift($transactions);

        if (empty($transaction)) {
            $transaction = commerce_payment_transaction_new('egopay', $order->order_id);
        }

        $transaction->instance_id = $payment_method['instance_id'];
        $transaction->remote_id = $data['sId'];
        $transaction->currency_code = $data['sCurrency'];
        $transaction->payload[REQUEST_TIME] = $data;
        $transaction->remote_status = $data['sStatus'];

        // If we didn't get an approval response code...
        switch ($data['sStatus']) {
            case 'Completed':
                $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
                $transaction->message = t('The payment has completed.');
                $order_status = 'completed';
                $transaction->amount = commerce_currency_decimal_to_amount($data['fAmount'], $data['sCurrency']);
                break;
            case 'Pending':
            case 'On Hold':
                $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
                $transaction->message = t('The payment has been holded or pending.');
                $order_status = 'pending';
                $transaction->amount = 0;
                break;
            case 'Canceled':
                $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
                $transaction->message = t('The payment has been canceled.');
                $order_status = 'canceled';
                $transaction->amount = 0;
                break;
        }

        commerce_payment_transaction_save($transaction);
        $order = commerce_order_status_update($order, $order_status);

        $data['sId'] = $transaction->transaction_id;

        watchdog('commerce_egopay', 'IPN processed for Order @order_number with ID @txn_id.', array(
            '@txn_id' => $data['sId'],
            '@order_number' => $order->order_number
        ), WATCHDOG_INFO);
    } else {
        watchdog('commerce_egopay',
            'Please enable CURL on your server to get transaction status reports from Egopay.com', null, WATCHDOG_INFO);
    }
}

function commerce_egopay_ipn_url($method_id = null)
{
    $parts = array(
        'commerce_egopay',
        'ipn',
    );

    if (!empty($method_id)) {
        $parts[] = $method_id;
    }

    return url(implode('/', $parts), array('absolute' => true));
}

function commerce_egopay_order_form($form, &$form_state, $order, $settings)
{
    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    $order_currency_code = $wrapper->commerce_order_total->currency_code->value();
    if ($order_currency_code == 'USD' || $order_currency_code == 'EUR') {
        $currency_code = $order_currency_code;
    } else {
        drupal_set_message(t('Egopay supports only two currencies (USD, EUR)'), 'error');
        return array();
    }

    $amount = $wrapper->commerce_order_total->amount->value();

    $oEgopay = new EgoPaySci(array(
        'store_id' => $settings['egopay_store_id'],
        'store_password' => $settings['egopay_store_password']
    ));
    $data = $oEgopay->createHash(array(
        'amount' => commerce_currency_amount_to_decimal(commerce_currency_convert($amount, $order_currency_code, $currency_code), $currency_code),
        'currency' => $currency_code,
        'description' => 'My payment for order id: ' . $order->order_id,
        'cf_1' => $order->order_id,
        'success_url' => $settings['success_url'],
        'fail_url' => $settings['fail_url'],
        'callback_url' => commerce_egopay_ipn_url($settings['payment_method'])
    ));
    $settings += array('payment_method' => '');

    $form['#action'] = EgoPaySci::EGOPAY_PAYMENT_URL;
    $form['hash'] = array('#type' => 'hidden', '#value' => $data);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Proceed to Egopay'));

    return $form;
}

/**
 * Returns an array of all possible currency codes.
 */
function commerce_egopay_currencies()
{
    return drupal_map_assoc(array('USD', "EUR"));
}
